/*
 *  AM2320 I2C Device Driver for Raspberry Pi
 *  Copyright (c) 2016 D.N. Amerasinghe (nivanthaka@gmail.com) 
 *
 *  References 
 *  http://lxr.free-electrons.com/source/include/uapi/asm-generic/i2c.h
 *  http://lxr.free-electrons.com/source/include/uapi/asm-generic/errno-base.h
 *  http://lxr.free-electrons.com/source/Documentation/i2c/writing-clients
 *  https://www.kernel.org/doc/Documentation/timers/timers-howto.txt
 */

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

#include <linux/module.h>
#include <linux/i2c.h>
#include <linux/err.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/jiffies.h>
#include <linux/hwmon.h>
#include <linux/hwmon-sysfs.h>
#include <linux/sysfs.h>

#define MPR_NAME "mpr"
#define MPR_I2C_ADDRESS 0x18

#define MPR_I2C_READ_CMD    0xAA

#define MPR_PSI_MIN	0
#define MPR_PSI_MAX	25

// Used for auto detection
static const unsigned short normal_i2c[] = {
    MPR_I2C_ADDRESS,
    I2C_CLIENT_END
};

struct mpr_device {
    struct i2c_client *client;
    struct mutex lock;
    u8 tbuffer[4]; // Used as the transaction buffer
    unsigned long last_updated;     //In jiffies
    struct attribute_group *group;
};

//------------- Prototypes ----------------------------//
static int mpr_i2c_read_data(struct i2c_client *client, u8 reg, u8 len);
static void mpr_update_data(struct i2c_client *client, s32 *data);
//-----------------------------------------------------//

//----------- Sensor sysfs ---------------------------------------------------//
static ssize_t pressure_input(struct device *dev, struct device_attribute *attr, 
        char *buf){
    struct i2c_client  *client  = to_i2c_client(dev);
    s32 reading = 0;
    mpr_update_data(client, &reading);

    return sprintf(buf, "%d\n", reading);
}

static SENSOR_DEVICE_ATTR(pressure1_input, S_IRUGO | S_IROTH, pressure_input, NULL,  0);

static struct attribute *mpr_attrs[] = {
//static struct device_attribute *mpr_attrs[] = {
    &sensor_dev_attr_pressure1_input.dev_attr.attr,
    NULL
};


static const struct attribute_group mpr_group = {
    .attrs = mpr_attrs,
};
//------------------------------------------------------------------------------//

static int mpr_i2c_detect(struct i2c_client *client, 
        struct i2c_board_info *info){

    //struct i2c_adapter *adapter = client->adapter;
    
    if(client->addr != MPR_I2C_ADDRESS)
        return -ENODEV;

    dev_info(&client->dev, "Detected AM2320\n");

    return 0;
}

// Reads the data from the device, currently it reads only 4 bytes
// TODO Improve this function to read any number of registers
// TODO CRC check
static int mpr_i2c_read_data(struct i2c_client *client, u8 reg, u8 len){
    int ret;
    struct mpr_device *mpr;
    
    mpr = i2c_get_clientdata(client);
    
    mpr->tbuffer[0] = reg;
    mpr->tbuffer[1] = 0x00;
    mpr->tbuffer[2] = 0x00;
    
    //Wakeup device
    mutex_lock(&mpr->lock);
    ret = i2c_master_send(client, mpr->tbuffer, 3);
    if(ret < 0){
        dev_err(&client->dev, "Failed to send commands\n");
        goto exit_lock;
    }
    // Delay as per datasheet
    usleep_range(5000, 6000);
    //ret = i2c_master_recv(device->client, device->tbuffer, len);
    ret = i2c_master_recv(client, mpr->tbuffer, 4);
    if(ret < 0){
        dev_err(&client->dev, "Failed to read data\n");
        goto exit_lock;
    }

    mutex_unlock(&mpr->lock);
    return 0;
    
exit_lock:
    mutex_unlock(&mpr->lock);
    return ret;
    
}

static void mpr_update_data(struct i2c_client *client,s32 *data){
    struct mpr_device *mpr;
    
    mpr = i2c_get_clientdata(client);
    if(mpr_i2c_read_data(client, MPR_I2C_READ_CMD, 4) == 0){
	s32 res = (mpr->tbuffer[1] << 16) | (mpr->tbuffer[2] << 8) | mpr->tbuffer[3];
	
	*data = res; 

        mpr->last_updated = jiffies;
    }
}

static int mpr_i2c_probe(struct i2c_client *client, 
        const struct i2c_device_id *id){

    struct i2c_adapter *adapter = client->adapter;
    struct mpr_device *mpr;

    dev_info(&client->dev, "Probe MPR\n");

    if(!i2c_check_functionality(adapter, I2C_FUNC_SMBUS_BYTE_DATA) && 
            !i2c_check_functionality(adapter, I2C_FUNC_SMBUS_I2C_BLOCK))
        return -EIO;
        
    mpr = devm_kzalloc(&client->dev, sizeof(struct mpr_device), GFP_KERNEL);
    if(!mpr){
        dev_err(&client->dev, "Cannot allocate memory\n");
        return -ENOMEM;
    }
    
    mpr->client = client;
    mpr->last_updated = 0;

    i2c_set_clientdata(client, mpr);
    mutex_init(&mpr->lock);

    //Registering sysfs files
    device_create_file(&client->dev,  (const struct device_attribute *)&sensor_dev_attr_pressure1_input);
        
    return 0;
}


static int mpr_i2c_remove(struct i2c_client *client){
    struct mpr_device *mpr;

    dev_info(&client->dev, "Remove MPR\n");
    
    mpr = i2c_get_clientdata(client);
    
    return 0;
}

static const struct i2c_device_id mpr_id[] = {
    {MPR_NAME, 0},
    {}
};

MODULE_DEVICE_TABLE(i2c, mpr_id);

static struct i2c_driver mpr_i2c_driver = {
    //.class = I2C_CLASS_HWMON,
    .driver = {
        .name = MPR_NAME
    },
    .id_table = mpr_id,
    .probe    = mpr_i2c_probe,
    .remove   = mpr_i2c_remove,
    .detect   = mpr_i2c_detect,
    .address_list = normal_i2c
};

module_i2c_driver(mpr_i2c_driver);

MODULE_AUTHOR("Przemek Recha <przemek.recha@gmail.com>");
MODULE_DESCRIPTION("MPR I2C bus driver");
MODULE_LICENSE("GPL");
