#include "mpr_fs.hpp"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "strato-frames/pressure_frame.hpp"
#include <memory>
#include <zmq.hpp>
#include <libconfig.h++>
#include <csignal>
#include <stdexcept>

class Application {
private:
  zmq::context_t                    context;
  std::shared_ptr<zmq::socket_t>    socket;
  std::shared_ptr<mpr_fs_t>         mpr;

  inline static bool running = false;
   static void termination_handler(int) {
    spdlog::info("Termination requested");
    running = false;
  }

  void init_log() {
    auto file_logger = spdlog::basic_logger_mt("mpr", "/var/log/balloon/mpr.log");
    spdlog::set_default_logger(file_logger);
    spdlog::default_logger()->flush_on(spdlog::level::level_enum::info);
  }

  void init_signals() {
    std::signal(SIGTERM, Application::termination_handler);
  }

  void init_socket() {
    libconfig::Config cfg;
    try {
      cfg.readFile("/etc/sp-config/sp.cfg");
    } catch (const libconfig::FileIOException &fioex) {
      throw std::runtime_error("I/O error while reading file.");
    } catch (const libconfig::ParseException &pex) {
      std::stringstream ss;
      ss << "Parse error at " << pex.getFile() << ":" << pex.getLine()
            << " - " << pex.getError() << std::endl;
      throw std::runtime_error(ss.str().c_str());
    }

    const libconfig::Setting& root = cfg.getRoot();
    int port;
    if (!root["mpr"].lookupValue("mpr_port", port)) {
      throw std::runtime_error("mpr port not defined in config.");
    }
    spdlog::info("Config parsed.");

    socket = std::make_shared<zmq::socket_t>(context, ZMQ_PUB);
    if (socket != nullptr) {
      socket->bind("tcp://*:" + std::to_string(port));
    } else {
      throw std::runtime_error("Error while creating socket.");
    }
    spdlog::info("Socket opened at: {}", port);
  }

public:
  Application() : context(1) {}    

  int exec() {
    running = true;
    init_log();
    init_signals();
    try {
      init_socket();
      mpr = std::make_shared<mpr_fs_t>();
    } catch (const std::exception& e) {
      spdlog::error(e.what());
      running = false;
      return -1;
    }

    while (running) {
      std::this_thread::sleep_for(std::chrono::seconds(1)); 

      try {
        pressure_frame_t frame(
          mpr->read_pressure()
        );
        spdlog::info("Pressure: {} Pa", frame.pressure);
        socket->send(frame_topic::pressure.begin(), frame_topic::pressure.end(), ZMQ_SNDMORE);
        socket->send(zmq::const_buffer(&frame, sizeof(pressure_frame_t)));
      } catch (const std::exception& e) {
        spdlog::error(e.what());
      }
    }
    return 0;
  }
};
