#pragma once

namespace mpr_driver {
  constexpr char new_device[]         = "/sys/class/i2c-adapter/i2c-1/new_device";
  constexpr char delete_device[]      = "/sys/class/i2c-adapter/i2c-1/delete_device";
  constexpr char pressure[]           = "/sys/class/i2c-adapter/i2c-1/1-0018/pressure1_input";
}

class mpr_fs_t {
public:
  mpr_fs_t();
  ~mpr_fs_t();

  float read_pressure();  // [Pa]

};