#include "mpr_fs.hpp"
#include <fstream>

mpr_fs_t::mpr_fs_t() {
  std::ofstream f(mpr_driver::new_device);
  f << "mpr 0x18";
  f.close();
}

mpr_fs_t::~mpr_fs_t() {
  std::ofstream f(mpr_driver::delete_device);
  f << "0x18";
  f.close();
}

float mpr_fs_t::read_pressure() {
  std::string val;
  std::ifstream f(mpr_driver::pressure);
  f >> val;
  f.close();
  int v = std::stoi(val);
  v = (v - 1677722) * 25;
  return static_cast<float>(v)/13421772.0*6894.75729;
}